package com.company;

import java.util.Scanner;

public class Main {

    private static final int COUNT = 5;
    private static String target = "tree";
    private static String[] targets = {"tree", "meet"};


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String[] strings = new String[COUNT];

        for (int i = 0; i < COUNT; i++) {
            strings[i] = scanner.nextLine();
        }
        for (int i = 0; i < strings.length; i++) {
            if ((contains(strings[i], target)) || (contains(strings[i], targets))) {
                System.out.println(strings[i]);
            }
        }
    }

    private static boolean contains(String string, String target) {
        String[] strings = string.split(" ");
        for (String s : strings) {
            if (s.equalsIgnoreCase(target)) {
                return true;
            }
        }
        return false;
    }

    private static boolean contains(String string, String[] targets) {
        String[] strings = string.split(" ");
        for (String str : strings) {
            for (String tar : targets) {
                if (str.equalsIgnoreCase(tar)) {
                    return true;
                }
            }
        }
        return false;
    }
}
